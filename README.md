# Arduino IDE Talk² Boards #

This repository contains the Talk² boards configuration for the Arduino IDE, also known as **boards.txt**.

## Automatic Installation ##

On the most recent version of Arduino IDE, third-party board configuration can be added with just a few clicks. Just follow the steps below to get the **boards.txt**, together with additional files and the binary version of the boards' bootloader, into your Arduino IDE.

### Board Manager URLs ###

The first thing to do is to add the Talk² Arduino IDE Boards URL: **http://talk2arduino.wisen.com.au/master/package_talk2.wisen.com_index.json**

#### Open your Arduino IDE and open the menu "File" -> "Preferences": ####

![ArduinoIDE_BoardManagerURL.png](https://bitbucket.org/repo/yBExBM/images/3290401582-ArduinoIDE_BoardManagerURL.png)

#### Now go to the menu "Tools" -> "Board" -> "Boards Manager...": ####

![ArduinoIDE_BoardManager.png](https://bitbucket.org/repo/yBExBM/images/2977663932-ArduinoIDE_BoardManager.png)

#### Select "Type": Contributed: ####

![ArduinoIDE_BoardManagerType.png](https://bitbucket.org/repo/yBExBM/images/447731691-ArduinoIDE_BoardManagerType.png)

#### Click on the "Talk2 AVR Boards" and hit "Install": ####

![ArduinoIDE_BoardManagerInstall.png](https://bitbucket.org/repo/yBExBM/images/159526520-ArduinoIDE_BoardManagerInstall.png)

#### Now you'll find the Talk² Board on your Arduino Boards menu: ####

![ArduinoIDE_BoardTalk2.png](https://bitbucket.org/repo/yBExBM/images/4287515095-ArduinoIDE_BoardTalk2.png)

## Manual Installation ##

If you're still using an older version of the Arduino IDE or just want to check the boards.txt contents, you can download the latest files from this repo and install on your Arduino Hardware location manually.